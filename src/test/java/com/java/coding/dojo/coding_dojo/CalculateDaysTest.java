package com.java.coding.dojo.coding_dojo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalculateDaysTest {

	String date1;
	
	String date2;
	
	@Before
	public void setUp(){
		date1 = "11/03/2016";
		date2 = "24/04/2020";
	}
	
	@Test(expected=InvalidDateException.class)
	public void testForValidDates() throws InvalidDateException{
		date1="";
		date2="";
		int days = CalculateDays.getDaysDifference(date1, date2);
		assertEquals(1, days);
		
	}
	
	@Test(expected=InvalidDateException.class)
	public void testForValidToDateAndInvalidFromDate() throws InvalidDateException{
		date1="11-03-2016";
		date2="24-04-2020";
		int days = CalculateDays.getDaysDifference(date1, date2);
		assertEquals(1, days);
		
	}
	
	@Test(expected=InvalidDateException.class)
	public void testForInvalidDateFormat() throws InvalidDateException{
		date1="";
		date2="24/04/2020";
		int days = CalculateDays.getDaysDifference(date1, date2);
		assertEquals(1, days);
		
	}
	
	@Test
	public void testGetDaysDifference() throws InvalidDateException{
		date1 = "11/04/2016";
		date2 = "12/04/2016";
		int days = CalculateDays.getDaysDifference(date1, date2);
		assertEquals(1, days);
	}

	@Test
	public void testGetDaysDifferenceForMonths() throws InvalidDateException{
		date1 = "11/03/2016";
		date2 = "11/11/2016";
		int days = CalculateDays.getDaysDifference(date1, date2);
		assertEquals(245, days);
	}

	@Test
	public void testGetDaysDifferenceForYears() throws InvalidDateException{
		date1 = "11/04/2016";
		date2 = "11/04/2020";
		int days = CalculateDays.getDaysDifference(date1, date2);
		assertEquals(1461 , days);
	}

	@Test
	public void testGetDaysDifferenceForYearsAndMonthsAndDays() throws InvalidDateException{
		//getting an inaccuracy of 1 day
		date1 = "1/03/2016";
		date2 = "11/11/2018";
		int days = CalculateDays.getDaysDifference(date1, date2);
		assertEquals(986 , days);
	}
}
