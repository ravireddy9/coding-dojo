package com.java.coding.dojo.coding_dojo;

public class CalculateDays {

	public static int getDaysDifference(String date1, String date2) throws InvalidDateException {
		
		if((date1.isEmpty() || null==date1) || (date2.isEmpty() || null== date2)){
			throw new InvalidDateException();
		}
		if(date1.split("/").length ==1 || date2.split("/").length == 1){
			throw new InvalidDateException();
		}
		int days = 0;
		int daysMonths = 0;
		int daysYears = 0;
		int[] inputDate1 = convertDateStringToInt(date1);
		int[] inputDate2 = convertDateStringToInt(date2);

		days = days + calculateDaysForDays(inputDate1[0], inputDate2[0]);
		System.out.println(days);
		daysMonths = daysMonths + calculateDaysForMonths(inputDate1[1], inputDate2[1]);
		System.out.println(daysMonths);
		daysYears = daysYears + calculateDaysForYears(inputDate1[2], inputDate2[2]);
		System.out.println(daysYears);



		return days + daysMonths + daysYears;
		
	}
	
	private static int[] convertDateStringToInt(String date){
		
		String[] splitDate = date.split("/");
		
		int[] intSplitDate = new int[splitDate.length];
		
		for(int i = 0; i < splitDate.length; i++){
			intSplitDate[i] = Integer.parseInt(splitDate[i]);
		}
		
		return intSplitDate;
		
	}

	private static int calculateDaysForDays(int startDay, int endDay){
		int days = 0;

		if(startDay > endDay) {
			days = days + (startDay - endDay);
		}
		else if(endDay > startDay){
			days = days + (endDay - startDay);
		}

		return days;
	}

	private static int calculateDaysForMonths(int startMonth, int endMonth){
		int days = 0;

		if(startMonth > endMonth) {
			for (int i = startMonth; i > endMonth; i--) {
				days = days + getDaysForMonth(i);
			}
		}
		else if(endMonth > startMonth){
			for (int i = startMonth; i < endMonth; i++) {
				days = days + getDaysForMonth(i);
			}
		}

		return days;
	}

	private static int calculateDaysForYears(int startYear, int endYear){
		int days = 0;

		for (int i = startYear; i < endYear; i++){
			//leap year - this logic may be inaccurate
			if(i%4 == 0) {
				days = days + 366;
			}
			else{
				days = days + 365;
			}
		}

		return days;
	}

	private static int getDaysForMonth(int month){
		switch(month){
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				return 31;
			case 4:
			case 6:
			case 9:
			case 11:
				return 30;
			case 2:
				return 28;
			default:
				return 0;
		}
	}

	
	public static void main(String args[]){
		
		System.out.println(convertDateStringToInt("23/02/2016"));
	}

}
